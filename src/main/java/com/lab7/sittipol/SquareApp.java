package com.lab7.sittipol;

public class SquareApp {
    public static void main(String[] args) {
        Square rect1 = new Square(10, 5);
        rect1.printSquareArea();
        Square rect2 = new Square(5, 3);
        rect2.printSquareArea();
        rect1.printSquarePerimeter();
        rect2.printSquarePerimeter();
    }
}
