package com.lab7.sittipol;

public class Square {
    //Attributes
    private int height;
    private int width;

    public Square(int height, int width) {//Construtor
        this.height = height;
        this.width = width;
    }
    public int printSquareArea() {
        int area = height * width;
        System.out.println(area);
        return area;
    }
    public int printSquarePerimeter() {
        int perimeter = height + width;
        System.out.println(perimeter); return perimeter;
    }
}