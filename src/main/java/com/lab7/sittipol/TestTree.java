package com.lab7.sittipol;

public class TestTree {
    //Attributes
    private String name;
    private int x;
    private int y;

    public TestTree(String name, int x, int y){ // Construtor
        this.name = name;
        this.x = x;
        this.y = y;
    }// Methodes
    public void print(){
        System.out.println("Tree: "+ name + " X:"+ x +" Y:"+ y);
    }
}