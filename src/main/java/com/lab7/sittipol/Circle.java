package com.lab7.sittipol;

public class Circle {
        //Attributes
        private double radian;

        public Circle(int radian) { // Construtor
            this.radian = radian;
        }
        public int printCircleRadian() {
            double area = 3.14 * (radian*radian);
            System.out.println(area);
            return (int)area;
        }
    
    
}
